# Random Experiments

This repository contains small Python programs for simulating random
experiments. The aim may be estimate probabilities, or some probabilistic
functions like mean or variance.

For knowing more about the individual experiments, please visit
[https://muddyhats.com](https://muddyhats.com)
