#!/usr/bin/python


__license__ = 'GPL v3'
__copyright__ = '2018, Jyothis V <jyothisv@gmx.com>'


import random as rand
from enum import Enum

class Color(Enum):
    RED=1
    BLUE=2


class RedBlue:
    def __init__(self, num_reds, num_blues):
        self.num_reds = num_reds
        self.num_blues = num_blues

        self.bag = [Color.RED] * num_reds + [Color.BLUE] * num_blues


    def prob_blues(self, num_picks, num_trials=1000):
        """Return the probability of getting 'i' blue balls when 'num_picks' balls are
        randomly taken from 'self.bag' without replacement. The return value is
        a list whose ith element is the probability of getting 'i' blue balls.
        """
        counts = [0] * (num_picks + 1)

        for i in range(num_trials):
            counts[self.count_blues(num_picks)] += 1

        return [x/num_trials for x in counts]


    def count_blues(self, num_picks):
        """Returns the number of blue balls observed when 'num_picks' balls are taken
        from 'self.bag' without replacement."""
        balls = rand.sample(self.bag, num_picks)
        return balls.count(Color.BLUE)


if __name__ == '__main__':
    rb = RedBlue(5, 4)

    # Compute the expectation and variance
    s = 0                       # For E[X]
    s2 = 0                      # For E[X^2]
    num_trials = int(1e5)
    num_picks = 3
    for i in range(num_trials):
        num_blues = rb.count_blues(num_picks)
        s += num_blues
        s2 += num_blues * num_blues
    mean = s/num_trials
    mean_x2 = s2/num_trials
    var = mean_x2 - mean*mean
    print("Mean = {0:.4f}, Variance = {1:.4f}".format(mean, var))
