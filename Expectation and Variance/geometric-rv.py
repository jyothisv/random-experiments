#!/usr/bin/python


__license__ = 'GPL v3'
__copyright__ = '2018, Jyothis V <jyothisv@gmx.com>'


import random as rand

class Geometric:
    def __init__(self, p):
        self.p = p


    def one_toss(self):
        """Toss the coin once."""
        return rand.random() <= self.p


    def geometric_rv(self):
        """Count the number of tosses until the first win."""
        count = 0
        while True:
            count += 1
            if self.one_toss():
                return count


if __name__ == '__main__':
    p = 0.25
    geometric = Geometric(p)

    analytical_mean = 1/p
    analytical_var = (1-p)/(p*p)

    print("Theoretical mean = {0}, variance = {1}".format(analytical_mean, analytical_var))

    # Experimentally compute expectation and variance.
    s, s2 = 0, 0
    num_trials = int(1e5)

    for i in range(num_trials):
        x = geometric.geometric_rv()
        s += x
        s2 += x*x

    mean = s/num_trials
    mean_x2 = s2/num_trials

    var = mean_x2 - mean*mean

    print("Estimated mean = {0:.4f}, variance = {1:.4f}".format(mean, var))
