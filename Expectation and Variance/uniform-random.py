#!/usr/bin/python


__license__ = 'GPL v3'
__copyright__ = '2018, Jyothis V <jyothisv@gmx.com>'


import random as rand


if __name__ == '__main__':
    analytical_mean = 0.5
    analytical_var = 1/12

    print("Theoretical mean = {0}, variance = {1}".format(analytical_mean, analytical_var))

    # Experimentally compute expectation and variance.
    s, s2 = 0, 0
    num_trials = int(1e5)

    for i in range(num_trials):
        x = rand.random()
        s += x
        s2 += x*x

    mean = s/num_trials
    mean_x2 = s2/num_trials

    var = mean_x2 - mean*mean

    print("Estimated mean = {0:.4f}, variance = {1:.4f}".format(mean, var))
