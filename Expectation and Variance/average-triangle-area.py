#! /usr/bin/python

# What is the average area of a triangle inscribed in a unit circle?

import random
import numpy as np

def make_one_triangle():
    t1 = random.uniform(0, 2*np.pi)
    t2 = random.uniform(0, 2*np.pi)
    t3 = random.uniform(0, 2*np.pi)

    v1 = np.array([np.cos(t1), np.sin(t1), 0])
    v2 = np.array([np.cos(t2), np.sin(t2), 0])
    v3 = np.array([np.cos(t3), np.sin(t3), 0])

    # print("sides: ", np.linalg.norm(v2-v1), np.linalg.norm(v3-v1), np.linalg.norm(v2-v3))

    area = np.linalg.norm(np.cross(v2-v1, v3-v1))

    # print(v1, v2, v3, area)

    return area


def experiment(num_trials):
    tot_area = 0

    for i in range(num_trials):
        tot_area += make_one_triangle()

    return tot_area / num_trials


if __name__ == '__main__':
    num_trials = 100000
    print("Average area = ", experiment(num_trials))
